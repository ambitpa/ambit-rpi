import requests
import config
import os
import shutil
import urllib.request
import datetime
import csv
import json
import time

# param
DEBUG_REPORT = 1
now = datetime.datetime.now()
config.timer_update_past = int(time.time())

# read raspberryPi ID
def readCpuId():
    # Extract serial from cpuinfo file
    cpu_key = "0"
    cpu_keyxx = ""

    try:
        f = open('/proc/cpuinfo', 'r')
        for i in f:
            if i[0:6] == 'Serial':
                cpu_key = i[10:26]
        f.close()
    except:
        cpu_key = "0"

    for i in range(len(cpu_key)):
        if cpu_key[i] is not '0':
            cpu_keyxx += str(cpu_key[i])

    return cpu_keyxx

# cek directory
def checkDirectory(get_path):
    if len(os.listdir(get_path)):
        return True
    else:
        return False

# Internet connection
def checkInternet(host='http://google.com'):
    try:
        urllib.request.urlopen(host)
        return True
    except:
        return False

# proses upload file
def uploadCsvFile(url, token, typeFile, device_id, path_sourche):
    try:
        # Open list dir
        list_dir = os.listdir(path_sourche)

        # Upload file
        if list_dir:
            url = url + '/custom/csv/upload?' + 'device_uid=' + \
                device_id + '&type=' + typeFile + '&access_token=' + token
            for entry_file in list_dir:
                # Creat new name file
                file_new_name = entry_file
                # Process upload
                file_upload = {'file': (file_new_name, open(
                    path_sourche + entry_file, 'rb'), 'text/csv')}
                response = requests.request("POST", url,  files=file_upload, headers={
                                            'User-Agent': 'raspberry pi'})
                # done report
                return response.status_code

        # eror report
        return False

    except:
        # eror report
        return False

# move file
def moveFile(path_sourche, path_target):
    # get date and time
    now = datetime.datetime.now()
    date_and_time = now.strftime("%Y_%m_%d_" + "_" + "%H_%M_%S")

    # Open list dir
    list_dir = os.listdir(path_sourche)

    for entry_file in list_dir:
        file_new_name = date_and_time + "_" + entry_file
        shutil.move(path_sourche + entry_file,
                    path_target + file_new_name)
    return 'Done !'

# Update config
def updateConfigCsv(data):
    with open(config.CONFIG_PATH, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',',
                                quotechar='\n', quoting=csv.QUOTE_MINIMAL)
        for key in data:
            if (key != 'id') and (key != 'device') and(key != 'owner') and (key != 'modified_on'):
                csv_writer.writerow([key]+[data[key]])
        
# Timer update
def timerUpdateConfig():
    now = int(time.time())
    if ((now - config.timer_update_past) >= config.TIMER_GET_CONFIG):
        config.timer_update_past = int(time.time())
        return 'Process'

    return 'Waiting ' + str(config.TIMER_GET_CONFIG - (now - config.timer_update_past)) + ' seconds !'