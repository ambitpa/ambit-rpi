#ambit test fetch

import multiprocessing
import time
import config
import apiService as ambit_api
import ambitSystem
import debug

def uploadingFile():
    while True:
        # Check internet
        if ambitSystem.checkInternet():
            debug.console('Checking internet', 'Internet up')
        else:
            debug.console('Checking internet',
                        'Internet down, Please check your connection')

        # check directory (spray log)
        if ambitSystem.checkDirectory(config.SPRAYLOG_PATH_PROCESS):
            debug.console('Checking file Spray log', 'found')
        else:
            debug.console('Checking file Spray log', 'Not found')

        # check directory (field map)
        if ambitSystem.checkDirectory(config.FIELDMAP_PATH_PROCESS):
            debug.console('Checking file Field map', 'found')
        else:
            debug.console('Checking file Field map', 'Not found')

        # upload file
        if (ambitSystem.checkDirectory(config.SPRAYLOG_PATH_PROCESS) or ambitSystem.checkDirectory(config.FIELDMAP_PATH_PROCESS)) and ambitSystem.checkInternet():
            # Upload spary log
            if ambitSystem.checkDirectory(config.SPRAYLOG_PATH_PROCESS):
                debug.console('Upload spraylog', 'Process...')
                report = ambitSystem.uploadCsvFile(
                    config.URL, config.TOKEN, 'spraylog', ambitSystem.readCpuId(), config.SPRAYLOG_PATH_PROCESS)
                if report == 403:
                    debug.console('Upload spraylog', str(report) +
                                ' You are not allowed to upload files')
                if report == 409:
                    debug.console('Upload spraylog', str(report) +
                                ' The request could not be completed due to duplicate csv file checksum')
                    message = ambitSystem.moveFile(
                        config.SPRAYLOG_PATH_PROCESS, config.SPRAYLOG_PATH_DONE)
                    debug.console('Move spraylog', message)
                if report == 200:
                    debug.console('Upload spraylog', str(report) + ' Succsess upload')
                    # Move file
                    message = ambitSystem.moveFile(
                        config.SPRAYLOG_PATH_PROCESS, config.SPRAYLOG_PATH_DONE)
                    debug.console('Move spraylog', message)
            # Upload fieldmap
            if ambitSystem.checkDirectory(config.FIELDMAP_PATH_PROCESS):
                debug.console('Upload fieldmap', 'Process...')   
                report = ambitSystem.uploadCsvFile(
                    config.URL, config.TOKEN, 'fieldmap', ambitSystem.readCpuId(), config.FIELDMAP_PATH_PROCESS)
                if report == 403:
                    debug.console('Upload fieldmap', str(report) +
                                ' You are not allowed to upload files')
                if report == 409:
                    debug.console('Upload fieldmap', str(report) +
                                ' The request could not be completed due to duplicate csv file checksum')
                    message = ambitSystem.moveFile(
                        config.FIELDMAP_PATH_PROCESS, config.FIELDMAP_PATH_DONE)
                    debug.console('Move fieldmap', message)            
                if report == 200:
                    debug.console('Upload fieldmap', str(report) + ' Succsess upload')
                    message = ambitSystem.moveFile(
                        config.FIELDMAP_PATH_PROCESS, config.FIELDMAP_PATH_DONE)
                    debug.console('Move fieldmap', message)

        print('>')
        time.sleep(1)                   


def getConfigFile():
    tmp_last_string = ''
    while True:
        # Update config
        tmp_string = ambitSystem.timerUpdateConfig()
        if tmp_string == 'Process':
            # Check connection internet
            if ambitSystem.checkInternet():
                debug.console('Update config CSV File: ', ambit_api.getConfig())
            else:
                debug.console('Checking internet', 'Update fail, Please check your connection')
        time.sleep(1)


# Multi thread config
process_1 = multiprocessing.Process(target = uploadingFile)
process_2 = multiprocessing.Process(target = getConfigFile)
process_1.start()
process_2.start()

print('================================================ Init done !')