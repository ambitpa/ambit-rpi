from reloadr import reloadr

@reloadr
class SomeThing:
    def do_stuff(self):
        pass

# Manual reload
SomeThing._reload()

# Automatic reload using filesystem notifications
SomeThing._start_watch_reload()

# Automatic reload in a thread every 1 second
SomeThing._start_timer_reload(1)
