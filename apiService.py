import config
import json
import requests
import ambitSystem

def getConfig():
    # Get first
    response = requests.request('GET', config.URL + '/custom/configurations?' +
                                'access_token=' + config.TOKEN + '&device_uid=' + ambitSystem.readCpuId(), headers={'User-Agent': 'raspberry pi'})
    # If succsess
    if response.status_code == 200:
        ambitSystem.updateConfigCsv(json.loads(response.text)['data'])
        return str(response.status_code) + ' CSV config file Update !'
    # Not succsess
    else:
        payload = json.loads(response.text)
        return str(response.status_code) + ' ' + payload["error"]["message"]